require 'course'

class Student
  attr_reader :courses, :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def push(old_course, *arg)
    @courses << old_course
  end

  def enroll(new_course)
    # if @course != nil
    #   @course.map do |old_course|
    #     raise_error if old_course.has_conflict?(new_course)
    #   end
    # end
    raise "error" if has_conflict?(new_course)
    push(new_course) unless @courses.include?(new_course)

    new_course.students << self
  end


  def course_load
    dept_n_credits = Hash.new(0)

    self.courses.map do |course_name|
      dept_n_credits[course_name.department] += course_name.credits
    end

    dept_n_credits
  end

  def has_conflict?(new_course)

    @courses.map do |old_course|
      return true if old_course.conflicts_with?(new_course)
    end

    return false
  end

end
